import { MainServer } from './MainServer';

export const server = new MainServer({
    port: +process.env.PORT || 8080,
});

async function execute() {
    try {
        const startingMessage = await server.start();
        console.log(startingMessage);
    } catch (err) {
        console.log('Cannot start server', err);
    }
}

execute().then().catch();
