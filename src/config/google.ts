import 'dotenv/config';

export const config = {
    GOOGLE_CLIENT_EMAIL: process.env.GOOGLE_CLIENT_EMAIL || '',
    GOOGLE_PRIVATE_KEY: process.env.GOOGLE_PRIVATE_KEY || '',
    BUCKET_NAME: process.env.BUCKET_NAME || '',
};
