import { RouterFunction } from '../../types';
import { fork } from 'child_process';
import crypto from 'crypto';
import { server } from '../../main';

export const uploadFile: RouterFunction = async (req, res) => {
    try {
        const file: Express.Multer.File = req.files['file'][0];
        const fileId = crypto.randomBytes(20).toString('hex');

        res.on('finish', async () => {
            const child = fork(__dirname + '../../../processes/fileParser');
            child.send(JSON.stringify({ buffer: file.buffer, fileId }));
            child.on('message', (mes: string) => {
                const parsedMessage = JSON.parse(mes);
                const { fileId, status } = parsedMessage;
                if (fileId && status) {
                    server.socket.emit(fileId, { status });
                }
            });
        });

        return res.json({ fileId });
    } catch (err) {
        console.log('uploadFile() err ', err);
        return res.status(500).send();
    }
};
