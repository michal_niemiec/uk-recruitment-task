import { RouterFunction } from '../../types';
import { prisma } from '../../prisma';

export const getFileData: RouterFunction = async (req, res) => {
    try {
        const { id } = req.params;

        const data = await prisma.user.findMany({ where: { fileId: id } });

        return res.json({ data });
    } catch (err) {
        console.log('getFileData() err ', err);
        return res.status(500).send();
    }
};
