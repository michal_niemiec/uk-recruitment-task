import * as bodyParser from 'body-parser';
import * as http from 'http';
import { IRouteObject, routes } from './routes';
import { Socket } from 'socket.io';
import morgan from 'morgan';
import express from 'express';
import helmet from 'helmet';

export interface IServerConfig {
    port: number;
}

export class MainServer {
    public server: http.Server;
    public app: express.Application;
    public socket: Socket;
    private port: number;

    constructor(serverConfig: IServerConfig) {
        this.app = express();
        this.server = http.createServer(this.app);
        this.port = serverConfig.port;
    }

    public start(): Promise<string> {
        return new Promise((resolve: (param: string) => void) => {
            this.socket = require('socket.io')(this.server);
            this.app.use(bodyParser.json({}));
            this.app.use(bodyParser.urlencoded({ extended: true }));
            this.app.use(morgan('combined'));
            this.app.disable('x-powered-by');
            this.app.use(helmet());
            this.server.listen(this.port, async () => {
                routes.forEach((route: IRouteObject) => {
                    this.app.use(`/${route.name}`, route.router);
                });
                resolve(`Server started on port  ${this.port}`);
            });
        });
    }

    public stop(): Promise<void> {
        return new Promise(async (resolve: () => void) => {
            this.server.close(resolve);
        });
    }
}
