import { NextFunction, Request, Response } from 'express';

export type RouterFunction = (
    req: Request,
    res: Response,
    next?: NextFunction,
) => Promise<Response | void>;

export enum GENDER {
    Male = 'Male',
    Female = 'Female',
}

export type User = {
    name: string;
    surname: string;
    gender: GENDER;
};

export enum UPLOAD_STATUS {
    STARTED = 'STARTED',
    PARSING = 'PARSING',
    SAVING_IN_DB = 'SAVING_IN_DB',
    UPLOADING = 'UPLOADING',
    COMPLETED = 'COMPLETED',
}
