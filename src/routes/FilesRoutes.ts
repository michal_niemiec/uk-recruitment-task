import { Router } from 'express';
import multer from 'multer';
import { getFileData, uploadFile } from '../controllers';

const upload = multer();
const router: Router = Router();

router.post('/upload', upload.fields([{ name: 'file', maxCount: 1 }]), uploadFile);
router.get('/:id', getFileData);

export const FilesRouter: Router = router;
