import { Router } from 'express';
import { FilesRouter } from './FilesRoutes';

export interface IRouteObject {
    name: string;
    router: Router;
}

export const routes = [
    {
        name: 'files',
        router: FilesRouter,
    },
];
