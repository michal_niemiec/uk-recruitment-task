import { prisma } from '../prisma';
import { FileParser } from '../services/FileParser/FileParser';
import GoogleStorage from '../services/GoogleStorage/GoogleStorage';
import { UPLOAD_STATUS } from '../types';

process.on('message', async (message: string) => {
    const { buffer, fileId } = JSON.parse(message);
    process.send(JSON.stringify({ fileId, status: UPLOAD_STATUS.STARTED }));
    process.send(JSON.stringify({ fileId, status: UPLOAD_STATUS.PARSING }));
    const { data, mimetype, extension } = await new FileParser(buffer).parseCsvFile();
    const spliced = data.slice(0, 5);

    process.send(JSON.stringify({ fileId, status: UPLOAD_STATUS.SAVING_IN_DB }));
    await prisma.user.createMany({ data: spliced.map((el) => ({ ...el, fileId })) });

    process.send(JSON.stringify({ fileId, status: UPLOAD_STATUS.UPLOADING }));
    await GoogleStorage.uploadToBucket(buffer, mimetype, `${fileId}.${extension}`);

    process.send(JSON.stringify({ fileId, status: UPLOAD_STATUS.COMPLETED }));
});
