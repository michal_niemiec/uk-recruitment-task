import { Storage, Bucket } from '@google-cloud/storage';
import { config } from '../../config/google';
import { FileParser } from '../FileParser/FileParser';

class GoogleStorage {
    private storage: Storage;
    private bucket: Bucket;
    constructor() {
        this.storage = new Storage({
            credentials: {
                client_email: config.GOOGLE_CLIENT_EMAIL,
                private_key: config.GOOGLE_PRIVATE_KEY.replace(/\\n/gm, '\n'),
            },
        });
        this.bucket = this.storage.bucket(config.BUCKET_NAME);
    }

    public uploadToBucket(buffer: Buffer, mimetype: string, path: string) {
        return new Promise<string>((resolve, reject) => {
            const stream = FileParser.parseBufferToStream(buffer);
            const file = this.bucket.file(path);
            stream
                .pipe(file.createWriteStream())
                .on('error', (err) => {
                    console.log(err);
                    reject(err);
                })
                .on('finish', async () => {
                    await file.setMetadata({ contentType: mimetype });
                    resolve(`gs://${config.BUCKET_NAME}/${path}`);
                });
        });
    }
}

export default new GoogleStorage();
