import { FileParser } from './FileParser';
import { Readable } from 'stream';
import fs from 'fs';

describe('FileParser', () => {
    describe('parseBufferToStream method', () => {
        it('should convert buffer to stream and return it', () => {
            const testBuffer = Buffer.from('This is Buffer for tests');
            const stream = FileParser.parseBufferToStream(testBuffer);

            expect(stream).toBeDefined();
            expect(stream).toBeInstanceOf(Readable);
        });
    });
    describe('parseCsvFile method', () => {
        it('should parse csv file correctly and return result', async () => {
            const buffer = fs.readFileSync(`${__dirname}/../../../MOCK_DATA.csv`);
            const result = await new FileParser(buffer).parseCsvFile();

            expect(result.extension).toBe('csv');
            expect(result.mimetype).toBe('text/plain');
            expect(result.data.length).toBe(1000);
        });
    });
});
