import csv from 'csv-parser';
import { Readable } from 'stream';
import { User } from '../../types';

interface IParseMethodResult {
    data: User[];
    mimetype: string;
    extension: string;
}

export class FileParser {
    private buffer: Buffer;

    constructor(buffer: Buffer) {
        this.buffer = buffer;
    }

    public parseCsvFile(): Promise<IParseMethodResult> {
        return new Promise((resolve, reject) => {
            const results = [];
            const readableStream = FileParser.parseBufferToStream(this.buffer);
            readableStream
                .pipe(csv())
                .on('data', (data) => results.push(data))
                .on('end', () => {
                    resolve({ data: results, mimetype: 'text/plain', extension: 'csv' });
                })
                .on('error', (error) => {
                    reject(error);
                });
        });
    }

    public static parseBufferToStream = (buffer: Buffer): NodeJS.ReadableStream => {
        const stream = new Readable();
        stream.push(Buffer.from(buffer));
        stream.push(null);

        return stream;
    };
}
