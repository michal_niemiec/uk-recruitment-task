## Prerequisites

-   docker
-   docker-compose
-   make (Windows - CMD, MacOS - brew, Ubuntu/WSL - apt) - optionally, you can also run commands without make.

## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:michal_niemiec/uk-recruitment-task.git
```

Go to the project directory

```bash
  cd my-project
```

If you have installed `make`

```bash
  make
```

Make will start docker services and run migrations.

If you don't have `make` you can run:

```bash
  docker-compose up -d
```

```bash
  docker-compose run api yarn migration:run
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`DATABASE_URL`

`BUCKET_NAME`

`GOOGLE_CLIENT_EMAIL`

`GOOGLE_PRIVATE_KEY`

`POSTGRES_USER`

`POSTGRES_PASSWORD`

`POSTGRES_DB`

## API Reference

`http://localhost:8080`

#### Upload csv file

```http
  POST /files/upload
```

| Parameter | Type   | Description                      |
| :-------- | :----- | :------------------------------- |
| `file`    | `File` | **Required**. CSV file with data |

#### Get data from file

```http
  GET /files/${id}
```

| Parameter | Type     | Description           |
| :-------- | :------- | :-------------------- |
| `id`      | `string` | **Required**. File id |

#### Example file

You can use prepared csv file to test API - `./MOCK_DATA.csv`
