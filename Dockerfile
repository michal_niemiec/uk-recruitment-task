FROM node:16 as release

RUN apt-get update && apt-get install -y vim

WORKDIR /app

COPY package.json package.json
COPY yarn.lock yarn.lock

RUN yarn

COPY . /app

EXPOSE 8080
CMD [ "yarn", "start" ]